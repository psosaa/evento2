import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import {
  UserService
} from './../services/user.service';
import {
  AngularFireDatabase
} from '@angular/fire/database';
import {
  Component,
  OnInit
} from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  itemRef: any;
  contacts = [];
  uid: string;
  constructor(private db: AngularFireDatabase, private user: UserService, 
    private alert:AlertController,
    private localNotifications: LocalNotifications) {
    this.uid = localStorage.getItem("uid")
  }

  ngOnInit() {
    this.itemRef = this.db.object('list/' + this.uid);
    console.log(this.uid);
    this.itemRef.snapshotChanges().subscribe(action => {
      this.notify()
      let data = action.payload.val();
      this.contacts = [];
      console.log(data);
      for (let k in data) {
        let user = data[k];
        user.key = k;
        console.log(user);
        this.contacts.push(user)
      }
    });

  }

 

  async deleteConfirm(key) {
    const alert = await this.alert.create({
      header: 'Espera!',
      message: 'Este contacto se eliminara de tu lista. Quieres continuar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Eliminar',
          handler: () => {
            console.log('Confirm Okay');
            this.db.database.ref('list/'+this.uid+'/'+key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
    notify(){
    // Schedule a single notification
this.localNotifications.schedule({
  id: 1,
  text: 'Single ILocalNotification',
  data: { secret: "" }
});


// Schedule multiple notifications
this.localNotifications.schedule([{
   id: 1,
   text: 'Multi ILocalNotification 1',

   data: { secret:"key" }
  },{
   id: 2,
   title: 'Local ILocalNotification Example',
   text: 'Multi ILocalNotification 2',
   icon: 'http://example.com/icon.png'
}]);


// Schedule delayed notification
this.localNotifications.schedule({
   text: 'Delayed ILocalNotification',
   trigger: {at: new Date(new Date().getTime() + 3600)},
   led: 'FF0000',
   sound: null
});
  }
  

}