// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyAN-hrtHZ_-Byx1xtqns8wagdTT3mSlIdg",
    authDomain: "evento-72909.firebaseapp.com",
    databaseURL: "https://evento-72909.firebaseio.com",
    projectId: "evento-72909",
    storageBucket: "evento-72909.appspot.com",
    messagingSenderId: "70966665154",
    appId: "1:70966665154:web:8957ef3467e7129fc8005b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
